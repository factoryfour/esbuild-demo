# esbuild Demo

Template for web applications configured with esbuild and typescript

### Sample Application includes the following: 
- a basic esbuild implementation that is highly dependent on typescript (no babel configuration)
- development workflow that supports live reloading (powered by live-server)
- ejs templated static assets
- linter support via eslint and prettier

### Todo
- adjust static parsing to support more than just one file 
- figure out browser compatibility to ensure we are not so restrictive with targets
- understand options for styling parsers
- figure out production settings for source maps and code splitting
- Detect ejs changes

### Notes on esbuild
- Key takeaway: *esbuild is very thin*
- Why a thin single maintainer build system is bad
  - most basic patterns have to be recreated
  - the single maintainer doesn't allow things the single maintainer doesn't want
  - example: the built-in esbuild watch server is fine but does not support live reloading
  - This is now an exercise left to the developer to implement https://github.com/evanw/esbuild/issues/802
- Pros of esbuild
  - it's wicked fast
  - a thin framework is not necessarily bad
    - esbuild : webpack :: react : angular
    - where the package does not have built-ins, the community will fill the gap
    - you aren't forced into patterns you don't like
- Migrating a large app might be a pain, but a new app could be worth starting with esbuild
