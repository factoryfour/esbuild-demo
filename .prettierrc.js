module.exports = {
    bracketSpacing: true,
    semi: true,
    singleQuote: true,
    trailingComma: 'all',
    tabWidth: 4,
    useTabs: true,
    arrowParens: "avoid",
    printWidth: 120,
};
