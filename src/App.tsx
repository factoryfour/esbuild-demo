import React from 'react';
import SampleTable from './SampleTable';

const App = function App() {
	return (
		<div
			style={{
				height: 200,
			}}
		>
			<span>Hello world from esbuild!</span>
			<SampleTable />
		</div>
	);
};

export default App;
