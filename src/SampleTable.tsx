import React from 'react';
import { Table, Row as RowLayout, TableColumn as Column, Pill } from '@factoryfour/react-patterns';

import randomStringData from './random-string-data';

export type StringRow = {
	id: string;
	[key: string]: string;
};

export type AbcdeRow = {
	id: string;
	a: string;
	b: string;
	c: string;
	d: string;
	e: string;
};

export type NestedRow = {
	id: string;
	obj: {
		a: string;
	};
	arr: string[];
	arrObj: { b: string | number }[];
	string: string;
};

export type NumberedRow = {
	id: string;
	num: number;
	a: string;
	b: string;
	c: string;
};

export const randomLengthString = (): string => {
	const maxLength = 40;
	const length = Math.ceil(Math.random() * maxLength);
	let str = '';

	// start with a random lowercase letter (avoid starting with a space)
	str += String.fromCharCode(97 + Math.floor(Math.random() * 26));

	for (let i = 1; i < length; i++) {
		const random = Math.floor(Math.random() * 27);
		if (random === 26) {
			str += ' ';
		} else {
			str += String.fromCharCode(97 + random); // random lowercase letter
		}
	}
	return str;
};

export const getColumnsAndData = (): {
	columns: Column<AbcdeRow>[];
	data: AbcdeRow[];
} => {
	const data: AbcdeRow[] = randomStringData.slice(0, 100);

	const columns: Column<AbcdeRow>[] = [
		{ key: 'a', renderCell: row => row.a, name: 'Column A' },
		{
			key: 'b',
			renderCell: row => row.b,
			name: 'Column B and the B is for Big Column with a Long Name',
		},
		{
			key: 'c',
			renderCell: row => (
				<RowLayout alignCenter fillHeight>
					<Pill
						text={row.b.length % 2 === 1 ? 'odd' : 'even'}
						textColor="white"
						pillColor={row.b.length % 2 === 1 ? '#606ED0' : '#60D089'}
					/>
				</RowLayout>
			),
			name: 'Parity',
		},
		{ key: 'd', renderCell: row => row.d, name: 'Column D' },
		{ key: 'e', renderCell: row => row.e, name: 'Column E' },
	];

	return { columns, data };
};

const SampleTable = function () {
	const { columns, data } = getColumnsAndData();
	return <Table columns={columns} data={data} title="Sample Table from react-patterns" showSearchBar={false} />;
};

export default SampleTable;
