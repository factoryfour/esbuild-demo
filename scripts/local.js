/* eslint-disable import/no-extraneous-dependencies,@typescript-eslint/no-var-requires,no-console */

/*
 * Inspired by https://github.com/evanw/esbuild/issues/802#issuecomment-955776480
 * */

const esbuild = require('esbuild');
const liveServer = require('live-server');
const { clean, config, parseStatic } = require('./app-common');

const PORT = 3000;
const OUTPUT = 'output';
const BUNDLE = `app.js`;

const { build } = esbuild;

const runBuild = async () => {
	await clean(OUTPUT);
	await parseStatic(OUTPUT, {
		bundle: BUNDLE,
	});

	await build({
		...config,
		outfile: `${OUTPUT}/${BUNDLE}`,
		plugins: [
			{
				name: 'start/end',
				setup(b) {
					b.onStart(() => {
						console.log('Build started');
					});
				},
			},
		],
		watch: {
			onRebuild(error) {
				if (error) {
					console.log(error.message, '\n');
					return;
				}
				console.log('Successfully built\n');
			},
		},
	});

	const params = {
		port: PORT,
		root: './output',
	};
	liveServer.start(params);
};

runBuild();
