/* eslint-disable @typescript-eslint/no-var-requires,import/no-extraneous-dependencies */
const fs = require('fs').promises;
const path = require('path');
const ejs = require('ejs');

const config = {
	entryPoints: [path.join(__dirname, '..', 'src', 'index.tsx')],
	bundle: true,
	minify: true,
	sourcemap: true,
	target: ['chrome58'],
};

/*
 * Clear static assets from build folder
 * */
const clean = async outputDirectory => {
	await fs.rmdir(path.join(__dirname, '..', outputDirectory), {
		recursive: true,
	});
	await fs.mkdir(path.join(__dirname, '..', outputDirectory));
};

const parseStatic = async (outputDirectory, data) => {
	const file = await ejs.renderFile(path.join(__dirname, '..', 'static', 'index.ejs'), data, { async: true });

	await fs.writeFile(path.join(__dirname, '..', outputDirectory, 'index.html'), file);
};

module.exports = {
	config,
	clean,
	parseStatic,
};
