module.exports = {
	root: true,
	parser: '@typescript-eslint/parser',
	extends: [
		'plugin:@typescript-eslint/recommended',
		'airbnb-base',
		'airbnb-base/rules/strict',
		'airbnb/rules/react',
		'plugin:prettier/recommended',
	],
	plugins: ['import', '@typescript-eslint'],
	rules: {
		'import/extensions': 'off',
		'import/no-extraneous-dependencies': [
			'error',
			{
				devDependencies: ['.storybook/**', 'stories/**', 'webpack.config.js'],
			},
		],
		'no-plusplus': ['error', { allowForLoopAfterthoughts: true }],
		'no-restricted-syntax': 'off',
		'no-undef': 'off',
		'no-unused-vars': 'off',
		'@typescript-eslint/no-unused-vars': ['error', { argsIgnorePattern: '^_' }],
		'@typescript-eslint/explicit-module-boundary-types': 'off',
		'no-use-before-define': 'off',
		'@typescript-eslint/no-use-before-define': ['error'],
		'react/jsx-filename-extension': ['warn', { extensions: ['.tsx', '.ts'] }],
		'react/jsx-props-no-spreading': 'off',
		'react/prop-types': 'off',
		'react/state-in-constructor': 'off',
		'react/require-default-props': 'off',
		'no-console': ['error', { allow: ['error'] }],
		camelcase: 'off',
		'func-names': 'off',
	},
	settings: {
		'import/parsers': {
			'@typescript-eslint/parser': ['.ts', '.tsx'],
		},
		'import/resolver': {
			typescript: {},
		},
	},
	overrides: [
		{
			files: '**.stories.tsx',
			rules: {
				'import/prefer-default-export': 'off',
			},
		},
		{
			files: '**.tsx',
			rules: {
				'@typescript-eslint/ban-types': [
					'error',
					{
						extendDefaults: true,
						types: {
							'{}': false,
						},
					},
				],
			},
		},
	],
};
